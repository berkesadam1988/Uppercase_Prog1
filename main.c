#include <stdio.h>
#include <stdlib.h>
#include "uppercase.c"


int main(int argc, char *argv[]) {
    if (argc > 1) {
        uppercase(argv);
    } else {
        printf("I couldn't find argument. The program exits.\n");
    }
    return 0;
}
